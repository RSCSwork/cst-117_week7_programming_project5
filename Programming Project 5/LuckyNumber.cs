﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Project_5
{
    public partial class LuckyNumber : Form
    {
        public LuckyNumber()
        {
            InitializeComponent();

            Form1 f = new Form1();

            numberLbl.Text = f.formulateLuckyNumber().ToString();

        }//ends lucky number

        private void LuckyNumber_Load(object sender, EventArgs e)
        {

        }

    }//ends class
}//ends namespace
