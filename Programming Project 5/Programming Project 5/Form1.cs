﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Project_5
{
    public partial class Form1 : Form
    {
        int luckyNumber = 0;
        int year;
        int month;
        int day;

        public Form1()
        {
            InitializeComponent();

            birthYearBox.DataSource = Enumerable.Range(1900, DateTime.Now.Year - 1900 + 1).ToList();
            birthYearBox.SelectedItem = DateTime.Now.Year;
            //int year = 0;
            bool getValue1 = int.TryParse(birthYearBox.SelectedItem.ToString(), out this.year);

            birthMonthBox.DataSource = CultureInfo.InvariantCulture.DateTimeFormat
                                                     .MonthNames.Take(12).ToList();
            birthMonthBox.SelectedItem = CultureInfo.InvariantCulture.DateTimeFormat
                                                    .MonthNames[DateTime.Now.AddMonths(-1).Month - 1];
            this.month = monthToInt(birthMonthBox.SelectedItem.ToString());

            //int day = 0;
            bool getValue2 = int.TryParse(birthDayBox.SelectedItem.ToString(), out this.day);

            colorBox.DataSource = fillColorBox();
            int colorValue = colorToInt(colorBox.SelectedItem.ToString());

            luckyNumber = formulateLuckyNumber(this.year, this.month, this.day, colorValue);

        }//ends public form

        public int monthToInt(string month)
        {
            switch (month)
            {
                case "January":
                    return 1;
                case "Febuary":
                    return 2;
                case "March":
                    return 3;
                case "April":
                    return 4;
                case "May":
                    return 5;
                case "June":
                    return 6;
                case "July":
                    return 7;
                case "August":
                    return 8;
                case "September":
                    return 9;
                case "October":
                    return 10;
                case "November":
                    return 11;
                case "December":
                    return 12;
                default:
                    return 0;
            }

            //return -1;
        }//ends month to int
        
        public List<int> daysInMonth(int year, int month)
        {
            List<int> daysInMonthList = new List<int>();

            for (int i = 0; i <= DateTime.DaysInMonth(year, month); i++)
            {
                daysInMonthList.Add(i);
            }//ends for loop

            return daysInMonthList;
        }//ends days in Month

        public int colorToInt(string color)
        {
            /*
             * We'll assign some random numbers
             * based on color, they're only semi-random
             * so that if someone runs the application
             * they'll always get the same lucky number
             * based on the calculation.
             */
            switch (color)
            {
                case "Red":
                    return 52;
                case "Orange":
                    return 80;
                case "Yellow":
                    return 26;
                case "Green":
                    return 93;
                case "Blue":
                    return 15;
                case "Purple":
                    return 67;
                case "Pink":
                    return 21;
                case "White":
                    return 78;
                case "Black":
                    return 34;
                case "Brown":
                    return 49;
                case "Grey":
                    return 11;
                case "Tan":
                    return 8;
                default:
                    return -1;
            }
            return -1;
        }//ends colorToInt

        public List<string> fillColorBox()
        {
            List<string> colorList = new List<string>();

            colorList.Add("Red");
            colorList.Add("Orange");
            colorList.Add("Yellow");
            colorList.Add("Green");
            colorList.Add("Blue");
            colorList.Add("Purple");
            colorList.Add("Pink");
            colorList.Add("White");
            colorList.Add("Black");
            colorList.Add("Brown");
            colorList.Add("Grey");
            colorList.Add("Black");

            return colorList;
        }

        public int formulateLuckyNumber(int year, int month, int day, int color)
        {
            return (((year * day) / month) / color);
        }


        private void LuckyNumBtn_Click(object sender, EventArgs e)
        {
            LuckyNumber form2 = new LuckyNumber();
            form2.ShowDialog();

        }//ends lucky number btn

        private void BirthMonthBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            birthDayBox.DataSource = daysInMonth(this.year, this.month);
            //int day = 0;
            bool getValue2 = int.TryParse(birthDayBox.SelectedItem.ToString(), out this.day);

            //return day;
        }



    }//ends partial class
}//ends namespace
