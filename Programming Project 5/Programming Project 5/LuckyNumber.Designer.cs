﻿namespace Programming_Project_5
{
    partial class LuckyNumber
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.announceLbl = new System.Windows.Forms.Label();
            this.numberLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // announceLbl
            // 
            this.announceLbl.AutoSize = true;
            this.announceLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.announceLbl.Location = new System.Drawing.Point(70, 38);
            this.announceLbl.Name = "announceLbl";
            this.announceLbl.Size = new System.Drawing.Size(412, 40);
            this.announceLbl.TabIndex = 0;
            this.announceLbl.Text = "Your Lucky Number is!!";
            // 
            // numberLbl
            // 
            this.numberLbl.AutoSize = true;
            this.numberLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberLbl.Location = new System.Drawing.Point(219, 127);
            this.numberLbl.Name = "numberLbl";
            this.numberLbl.Size = new System.Drawing.Size(119, 40);
            this.numberLbl.TabIndex = 1;
            this.numberLbl.Text = "label1";
            // 
            // LuckyNumber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 220);
            this.Controls.Add(this.numberLbl);
            this.Controls.Add(this.announceLbl);
            this.Name = "LuckyNumber";
            this.Text = "LuckyNumber";
            this.Load += new System.EventHandler(this.LuckyNumber_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label announceLbl;
        private System.Windows.Forms.Label numberLbl;
    }
}