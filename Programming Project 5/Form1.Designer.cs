﻿namespace Programming_Project_5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.birthYearBox = new System.Windows.Forms.ComboBox();
            this.birthMonthBox = new System.Windows.Forms.ComboBox();
            this.birthDayBox = new System.Windows.Forms.ComboBox();
            this.colorBox = new System.Windows.Forms.ComboBox();
            this.birthYearLbl = new System.Windows.Forms.Label();
            this.birthMonthLbl = new System.Windows.Forms.Label();
            this.birthDayLbl = new System.Windows.Forms.Label();
            this.colorLbl = new System.Windows.Forms.Label();
            this.luckyNumBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // birthYearBox
            // 
            this.birthYearBox.FormattingEnabled = true;
            this.birthYearBox.Location = new System.Drawing.Point(299, 79);
            this.birthYearBox.Name = "birthYearBox";
            this.birthYearBox.Size = new System.Drawing.Size(155, 28);
            this.birthYearBox.TabIndex = 0;
            this.birthYearBox.SelectedIndexChanged += new System.EventHandler(this.BirthYearBox_SelectedIndexChanged);
            // 
            // birthMonthBox
            // 
            this.birthMonthBox.FormattingEnabled = true;
            this.birthMonthBox.Location = new System.Drawing.Point(299, 137);
            this.birthMonthBox.Name = "birthMonthBox";
            this.birthMonthBox.Size = new System.Drawing.Size(155, 28);
            this.birthMonthBox.TabIndex = 1;
            this.birthMonthBox.SelectedIndexChanged += new System.EventHandler(this.BirthMonthBox_SelectedIndexChanged);
            // 
            // birthDayBox
            // 
            this.birthDayBox.FormattingEnabled = true;
            this.birthDayBox.Location = new System.Drawing.Point(299, 195);
            this.birthDayBox.Name = "birthDayBox";
            this.birthDayBox.Size = new System.Drawing.Size(155, 28);
            this.birthDayBox.TabIndex = 2;
            this.birthDayBox.SelectedIndexChanged += new System.EventHandler(this.BirthDayBox_SelectedIndexChanged);
            // 
            // colorBox
            // 
            this.colorBox.FormattingEnabled = true;
            this.colorBox.Location = new System.Drawing.Point(299, 257);
            this.colorBox.Name = "colorBox";
            this.colorBox.Size = new System.Drawing.Size(155, 28);
            this.colorBox.TabIndex = 3;
            this.colorBox.SelectedIndexChanged += new System.EventHandler(this.ColorBox_SelectedIndexChanged);
            // 
            // birthYearLbl
            // 
            this.birthYearLbl.AutoSize = true;
            this.birthYearLbl.Location = new System.Drawing.Point(68, 87);
            this.birthYearLbl.Name = "birthYearLbl";
            this.birthYearLbl.Size = new System.Drawing.Size(161, 20);
            this.birthYearLbl.TabIndex = 4;
            this.birthYearLbl.Text = "Select your birth year:";
            // 
            // birthMonthLbl
            // 
            this.birthMonthLbl.AutoSize = true;
            this.birthMonthLbl.Location = new System.Drawing.Point(68, 145);
            this.birthMonthLbl.Name = "birthMonthLbl";
            this.birthMonthLbl.Size = new System.Drawing.Size(176, 20);
            this.birthMonthLbl.TabIndex = 5;
            this.birthMonthLbl.Text = "Select your birth month:";
            // 
            // birthDayLbl
            // 
            this.birthDayLbl.AutoSize = true;
            this.birthDayLbl.Location = new System.Drawing.Point(68, 203);
            this.birthDayLbl.Name = "birthDayLbl";
            this.birthDayLbl.Size = new System.Drawing.Size(156, 20);
            this.birthDayLbl.TabIndex = 6;
            this.birthDayLbl.Text = "Select your birth day:";
            // 
            // colorLbl
            // 
            this.colorLbl.AutoSize = true;
            this.colorLbl.Location = new System.Drawing.Point(68, 265);
            this.colorLbl.Name = "colorLbl";
            this.colorLbl.Size = new System.Drawing.Size(186, 20);
            this.colorLbl.TabIndex = 7;
            this.colorLbl.Text = "Select your favorite color:";
            // 
            // luckyNumBtn
            // 
            this.luckyNumBtn.Location = new System.Drawing.Point(72, 334);
            this.luckyNumBtn.Name = "luckyNumBtn";
            this.luckyNumBtn.Size = new System.Drawing.Size(382, 50);
            this.luckyNumBtn.TabIndex = 8;
            this.luckyNumBtn.Text = "Get Your Lucky Number!";
            this.luckyNumBtn.UseVisualStyleBackColor = true;
            this.luckyNumBtn.Click += new System.EventHandler(this.LuckyNumBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 450);
            this.Controls.Add(this.luckyNumBtn);
            this.Controls.Add(this.colorLbl);
            this.Controls.Add(this.birthDayLbl);
            this.Controls.Add(this.birthMonthLbl);
            this.Controls.Add(this.birthYearLbl);
            this.Controls.Add(this.colorBox);
            this.Controls.Add(this.birthDayBox);
            this.Controls.Add(this.birthMonthBox);
            this.Controls.Add(this.birthYearBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox birthYearBox;
        private System.Windows.Forms.ComboBox birthMonthBox;
        private System.Windows.Forms.ComboBox birthDayBox;
        private System.Windows.Forms.ComboBox colorBox;
        private System.Windows.Forms.Label birthYearLbl;
        private System.Windows.Forms.Label birthMonthLbl;
        private System.Windows.Forms.Label birthDayLbl;
        private System.Windows.Forms.Label colorLbl;
        private System.Windows.Forms.Button luckyNumBtn;
    }
}

